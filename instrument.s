; Ben Companeitz
; ECE 425 - Dr. Mirzaei
; Final Project

; >>> instrument.s <<<
; The contained methods are primarily concerned with
;	parsing encoded notes or "lines" and properly
;	starting, setting, and stopping TIMER1 and TIMER0
; 	which handle frequency generation and quick note
;	switching (or "rippling") for multinote sound.
; Frequencies are mapped to cycle counts, which are
;	then used as a lookup table for notes.
; Because this board/chip don't possess a capability
;	for multichannel sound generation, the next best
;	way to play multiple notes simultaneously is to
;	quickly switch between the frequencies. TIMER0
;	handles this functionality.
; The main function to check out is <play_line>.

				AREA	instrument, CODE, READONLY								
ENTRY

; ------------------------------------------------------
				GLOBAL	instr_config
instr_config	STMFA	sp!, {r0-r3,lr}
; -------------------------------------------------
; TIMER1 Related Constants
TIMER1TCR		EQU	0xE0008004	; Timer Control Register
								; bit0: 1 to enable
								; bit1: reset all counting w/1
TIMER1CCR		EQU	0xE0008070	; Count Control Register
								; bit 1:0 - 0 timer inc on rising
								; bit 3:2 - input for counting. DC
								; bit 7:4 - 0
TIMER1COUNT		EQU	0xE0008008	; Timer Counter 
TIMER1PRES		EQU	0xE000800C	; Prescale Register, scales sysclock
								; Full word to set as scaling
TIMER1PCOUNT	EQU	0xE0008010	; Prescale Counter...
								; Runs on PCLK and runs up to
								; [prescale reg] then returns to 0.
TIMER1MR0		EQU	0xE0008018	; Match Registers...
TIMER1MR1		EQU	0xE000801C	; Can gen interrupt, reset, stop, etc
TIMER1MR2		EQU	0xE0008020
TIMER1MR3		EQU	0xE0008024
TIMER1MC		EQU	0xE0008014	; Match Control Register
								; 3 bits per match register
								; bit0: MR0 interrupt enable
								; bit1: MR0 reset enable
								; bit2: MR0 stop enable
								; bit3-5: repeats for MR1... etc
								; bits12-31: 0
TIMER1IR        EQU 0xE0008000  ; Interrupt Register (EACH TOGGLES!!)
                                ; bit0-3: match channels 0-3
                                ; bit4-7: capture channels 0-3
TIMER1EMR		EQU 0xE000803C	; External Match Register
								; bits 0-3: whether EM0-3 active
								; bits5:4: EM0 behavior...
								;	00 n/a
								;	01 clear to 0
								;	10 set to 1
								;	11 toggle!!!!

; TIMER1_CFG	; This will be the frequency generator
				
				LDR	r0, =TIMER1CCR
				MOV	r1, #0x0
				STR	r1, [r0]	; set as timer
				LDR	r0, =TIMER1MR0
				LDR	r1, =50
				STR	r1, [r0]	; set pulse width
				LDR	r0, =TIMER1MR1
				LDR	r1, =100
				STR	r1, [r0]	; set frequency
                LDR r0, =TIMER1MC
				LDR r1, =0x19
                STR r1, [r0]    ; RES: mr1, INT: mr0,1
								; - 000 000 011 001
				LDR	r0, =TIMER1TCR
				MOV	r1, #0x2	
				STR	r1, [r0]	; reset & stop

; -------------------------------------------------
; TIMER0 Related Constants
TIMER0TCR		EQU	0xE0004004	; Timer Control Register
								; bit0: 1 to enable
								; bit1: reset all counting w/1
TIMER0CCR		EQU	0xE0004070	; Count Control Register
								; bit 1:0 - 0 timer inc on rising
								; bit 3:2 - input for counting. DC
								; bit 7:4 - 0
TIMER0COUNT		EQU	0xE0004008	; Timer Counter 
TIMER0PRES		EQU	0xE000400C	; Prescale Register, scales sysclock
								; Full word to set as scaling
TIMER0PCOUNT	EQU	0xE0004010	; Prescale Counter...
								; Runs on PCLK and runs up to
								; [prescale reg] then returns to 0.
TIMER0MR0		EQU	0xE0004018	; Match Registers...
TIMER0MR1		EQU	0xE000401C	; Can gen interrupt, reset, stop, etc
TIMER0MR2		EQU	0xE0004020
TIMER0MR3		EQU	0xE0004024
TIMER0MC		EQU	0xE0004014	; Match Control Register
								; 3 bits per match register
								; bit0: MR0 interrupt enable
								; bit1: MR0 reset enable
								; bit2: MR0 stop enable
								; bit3-5: repeats for MR1... etc
								; bits12-31: 0
TIMER0IR        EQU 0xE0004000  ; Interrupt Register (EACH TOGGLES!!)
                                ; bit0-3: match channels 0-3
                                ; bit4-7: capture channels 0-3
TIMER0EMR		EQU 0xE000403C	; External Match Register
								; bits 0-3: whether EM0-3 active
								; bits5:4: EM0 behavior...
								;	00 n/a
								;	01 clear to 0
								;	10 set to 1
								;	11 toggle!!!!

; TIMER0_CFG	; This will be the ripple chord counter
				
				LDR	r0, =TIMER0CCR
				MOV	r1, #0x0
				STR	r1, [r0]	; set as timer
				LDR	r0, =TIMER0MR0
				LDR	r1, =50000
				STR	r1, [r0]	; set length of note in
								; ripple chord
                LDR r0, =TIMER0MC
				LDR r1, =0x3
                STR r1, [r0]    ; RES: mr0, INT: mr0
								; - 000 000 000 011
				LDR	r0, =TIMER0TCR
				MOV	r1, #0x2	
				STR	r1, [r0]	; reset & stop

; -------------------------------------------------
				LDMFA	sp!,{r0-r3,pc}
; -------------------------------------------------


; INSTRUMENT FUNCTIONS!
; =================================================
; -------------------------------------------------
CURRENT_LINE	DCD	0x40007FFC
DURATION_UNIT	DCD	0x4000

; -------------------------------------------------
; SUBROUTINE: play_line
; r0: a formatted word of notes
				GLOBAL	play_line
play_line		STMFA	sp!, {r0-r3,lr}
				; check how many notes
				MOV	r1, r0
				AND	r1, r1, #0x3	; mask # of notes
				ADD	pc, pc, r1, LSL #2
				NOP
				B	note_delay
				B	single
				B	chord
				B	chord
				
				;; 0? skip to delay
single			;; 1? press_note
				MOV	r3, #12
				MOV	r1, r0, LSR #2
				AND	r1, r1, #0x7	; mask octave 0
				MOV	r2, r0, LSR	#5
				AND	r2, r2, #0xF	; mask note 0
				MUL	r1, r3, r1		; calculate octave offset
				ADD r1, r2, r1		; add note offset
				BL	R2_get_cycles	; get the cycle number of note
				BL	press_note
				B	note_delay
chord			;; 2-3? engage the rippler
				;;; send current line to the place...
				LDR	r1, CURRENT_LINE
				BIC	r0, r0, #0x80000000	; make sure reserved clr!
				STR	r0, [r1]
				MOV	r2, #0
				STR	r2, [r1, #-4]		; zero current_note!
				;;; activate T0 (T0's IRQ will press
				;;; the notes for you!)
				LDR	r1, =TIMER0TCR
				MOV	r3, #0x1	
				STR	r3, [r1]	; start	
note_delay		; delay!
				LDR	r1, DURATION_UNIT
				MOV	r0, r0, LSR #23
				;AND	r0, r0, #0xFF	; mask delay NO NEED
				MUL	r0, r1, r0
				MOV	r0, r0, LSR #3	; b/c delay in code is Q3
				BL	delayR0
				BL	release_note
				LDMFA	sp!, {r0-r3,pc}
; END SUBROUTINE

; -------------------------------------------------
; SUBROUTINE: press_note
; r0: cycle time
				GLOBAL	press_note
press_note		STMFA	sp!, {r0-r2,lr}
				
				LDR	r1, =TIMER1MR1
				STR	r2, [r1]	; set frequency
				
				MOV	r0, r2, LSR #1	; div by 2 b/c
									; toggles every
									; match.
				
				LDR	r1, =TIMER1MR0
				STR	r0, [r1]	; set pulse width
				
				LDR	r0, =TIMER1COUNT
				MOV	r1, #0x0
				STR	r1, [r0]	; reset TC
				LDR	r1, =TIMER1TCR
				MOV	r0, #0x1	
				STR	r0, [r1]	; start		
				
				LDMFA	sp!, {r0-r2,pc}
; END SUBROUTINE

; -------------------------------------------------
; SUBROUTINE: release_note
				GLOBAL	release_note
release_note	STMFA	sp!, {r0-r3,lr}
				
				LDR	r0, =TIMER1TCR
				MOV	r1, #0x2	
				STR	r1, [r0]	; reset & stop freq gen
				LDR	r0, =TIMER0TCR
				MOV	r1, #0x2	
				STR	r1, [r0]	; reset & stop rippler
				
				LDMFA	sp!, {r0-r3,pc}
; END SUBROUTINE

; -------------------------------------------------
;; Delay for certain number of microseconds
;; SUBROUTINE							;# Cycles/Op
delay1			STMFA	sp!, {r0, lr}	;2 
				MOV		r0, #2			;1 
				MOV		r0, r0			;1
delay1_loop		SUBS	r0, r0, #1		;;1	}2*2=4
				BNE		delay1_loop		;;1 }
				LDMFA	sp!, {r0, pc}	;2 		Total: 10
;; END SUBROUTINE
;; SUBROUTINE
				GLOBAL	delayR0
delayR0			STMFA	sp!, {r0, lr}	;2
delayR0_loop	BL		delay1			;;10 }
				SUBS	r0, r0, #1		;;1  }r0*12	
				BNE		delayR0_loop	;;1  }
				LDMFA	sp!, {r0, pc}	;2		Total: 12*r0+4
;; END SUBROUTINE

; -------------------------------------------------
; SUBROUTINE: get_cycles (only b/c rippler needs access)
; r1: offset, returns cycles in R2
				GLOBAL	R2_get_cycles
R2_get_cycles	STMFA	sp!, {r0-r1,lr}
				LDR	r0, =octave0
				LDR	r2, [r0, r1, LSL #2]	; get the cycle number of note
				LDMFA	sp!, {r0-r1,pc}
; END SUBROUTINE

; -------------------------------------------------
; Notes: There are 12 distinct notes per octave, the 
; cycle times of which are listed in order from lowest 
; to highest here:

octave0		DCD	366972
			DCD	346320
			DCD	326886
			DCD	308562
			DCD	291262
			DCD	274914
			DCD	259459
			DCD	244897
			DCD	231169
			DCD	218181
			DCD	205937
			DCD	194363
octave1		DCD	183458
			DCD	173160
			DCD	163443
			DCD	154281
			DCD	145613
			DCD	137441
			DCD	129729
			DCD	122448
			DCD	115573
			DCD	109090
			DCD	102968
			DCD	97189
octave2		DCD	91736
			DCD	86586
			DCD	81727
			DCD	77140
			DCD	72811
			DCD	68724
			DCD	64864
			DCD	61224
			DCD	57789
			DCD	54545
			DCD	51484
			DCD	48594
octave3		DCD	45866
			DCD	43293
			DCD	40863
			DCD	38569
			DCD	36404
			DCD	34361
			DCD	32433
			DCD	30612
			DCD	28894
			DCD	27272
			DCD	25742
			DCD	24297
octave4		DCD	22933
			DCD	21646
			DCD	20431
			DCD	19284
			DCD	18202
			DCD	17180
			DCD	16216
			DCD	15306
			DCD	14447
			DCD	13636
			DCD	12870
			DCD	12148
octave5		DCD	11466
			DCD	10823
			DCD	10215
			DCD	9642
			DCD	9101
			DCD	8590
			DCD	8108
			DCD	7653
			DCD	7223
			DCD	6818
			DCD	6435
			DCD	6074
octave6		DCD	5733
			DCD	5411
			DCD	5107
			DCD	4821
			DCD	4550
			DCD	4295
			DCD	4054
			DCD	3826
			DCD	3611
			DCD	3409
			DCD	3217
			DCD	3037
octave7		DCD	2866
			DCD	2705
			DCD	2553
			DCD	2410
			DCD	2275
			DCD	2147
			DCD	2027
			DCD	1913
			DCD	1805
			DCD	1704
			DCD	1608
			DCD	1518
			
			END
			