; Ben Companeitz
; ECE 425 - Dr. Mirzaei
; Final Project

; >>> main.s <<<
; Contains the encoded song data and loops that iterate
; 	and play it on the speaker.
; The song played is "Megaman 2 Intro Theme" by Capcom.

				GLOBAL	main_code
				AREA	main, CODE, READONLY
				; Get instrument capabilities
				IMPORT	press_note
				IMPORT	release_note
				IMPORT	play_line
ENTRY
main_code
				
				B	pA
				
pA				LDR	r1, =part0
loop_0			LDR	r0, [r1], #4
				CMP	r0, #0
				BEQ	pB
				BL	play_line
				B	loop_0

pB				LDR	r1, =part1
loop_1			LDR	r0, [r1], #4
				CMP	r0, #0
				BEQ	pA
				BL	play_line
				B	loop_1
				
stop			B	stop



; -------------------------------------------------
; Each word represents a particular note or rest. The format
; of the bits within is as follows:

; [31:reserved][30-23:delay][22-19:n2][18-16:o2][15-12:n1][11-9:o1][8-5:n0][4-2:o0][1-0:#notes]
; reserved: always set as zero. used by ripple chord timer
; delay: How many units of set time expire for this note/chord (Q3)
; nx: note within octave (low to high notes)
; ox: particular octave from (low to high notes)
; #notes: 	0: rest
;			1: single
;			2: double-stop
;			3: chord

; Version with only single notes			
; -------------------------------------------------
partA		DCD	0x02000019	; C6	;-
			DCD	0x02000000	; rest
			DCD	0x02000019	; C6
			DCD	0x02000019	; C6 
			DCD	0x02000019	; C6 	;-
			DCD	0x02000000	; rest
			DCD	0x02000019	; C6 
			DCD	0x02000019	; C6 	;-
			DCD	0x04000019	; C6 
			
			DCD	0x04000059	; D6 
			
			DCD	0x04000000	; rest	;-
			DCD	0x1E000155	; B5
			
									;-
			
									;-
									
									;-
									
									;-
			DCD	0x02000000	; rest	
			DCD	0x04000155	; B5
			DCD	0x02000115	; A5	;-
			DCD	0x02000000	; rest
			DCD	0x02000115	; A5
			DCD	0x02000115	; A5
			DCD	0x04000115	; A5	;-
			DCD	0x040000B5	; F5
			DCD	0x04000000	; rest	;-
			DCD	0x040000B5	; F5
			DCD	0x04000000	; rest	;-
			DCD	0x0C0000F5	; G5
									;-
									
			DCD	0x04000000	; rest	;-
			DCD	0x040000F5	; G5
			DCD	0x04000115	; A5	;-		
			DCD	0x06000155	; B5
									;-
			DCD	0x06000000	; rest
			DCD	0x0
			
			
; -------------------------------------------------	
partB		DCD	0x02000019	; C6	;-
			DCD	0x02000000	; rest
			DCD	0x02000019	; C6
			DCD	0x02000019	; C6 
			DCD	0x02000019	; C6 	;-
			DCD	0x02000000	; rest
			DCD	0x02000019	; C6 
			DCD	0x02000019	; C6 	;-
			DCD	0x04000019	; C6 
			
			DCD	0x04000059	; D6 
			
			DCD	0x04000000	; rest	;-
			DCD	0x1E000155	; B5
			
									;-
			
									;-
									
									;-
									
									;-
			DCD	0x02000000	; rest	
			DCD	0x04000155	; B5
			DCD	0x02000115	; A5	;-
			DCD	0x02000000	; rest
			DCD	0x02000115	; A5
			DCD	0x02000115	; A5
			DCD	0x04000115	; A5	;-
			DCD	0x04000155	; B5
			DCD	0x04000000	; rest	;-
			DCD	0x04000155	; B5
			DCD	0x04000000	; rest	;-
			DCD	0x0C000019	; C6
									;-
									
			DCD	0x08000000	; rest	;-
			
			DCD	0x020000FD	; G7	;-			
			DCD	0x020000BD	; F7
			DCD	0x0200007D	; E7	
			DCD	0x020000BD	; F7	
			DCD	0x0200009D	; E#7	;-
			DCD	0x0200007D	; E7
			DCD	0x0200005D	; D#7
			DCD	0x0200003D	; D
			
			DCD	0x0

; -------------------------------------------------
; =================================================

; Version with double-notes!
; -------------------------------------------------
part0		DCD	0x02007A1A	; C6	;-
			DCD	0x02000000	; rest
			DCD	0x02007A1A	; C6
			DCD	0x02007A1A	; C6 
			DCD	0x02007A1A	; C6 	;-
			DCD	0x02000000	; rest
			DCD	0x02007A1A	; C6 
			DCD	0x02007A1A	; C6 	;-
			DCD	0x04007A1A	; C6 
			
			DCD	0x04007A5A	; D6 
			
			DCD	0x04000000	; rest	;-
			DCD	0x1E005B56	; B5
			
									;-
			
									;-
									
									;-
									
									;-
			DCD	0x02000000	; rest	
			DCD	0x04007B56	; B5
			DCD	0x02003B16	; A5	;-
			DCD	0x02000000	; rest
			DCD	0x02003B16	; A5
			DCD	0x02003B16	; A5
			DCD	0x04003B16	; A5	;-
			DCD	0x04002AB6	; F5
			DCD	0x04000000	; rest	;-
			DCD	0x04002AB6	; F5
			DCD	0x04000000	; rest	;-
			DCD	0x0C004AF6	; G5
									;-
									
			DCD	0x04000000	; rest	;-
			DCD	0x04002AF6	; G5
			DCD	0x04003B16	; A5	;-		
			DCD	0x06005B56	; B5
									;-
			DCD	0x06000000	; rest
			DCD	0x0
			
; -------------------------------------------------	
part1		DCD	0x02007A1A	; C6	;-
			DCD	0x02000000	; rest
			DCD	0x02007A1A	; C6
			DCD	0x02007A1A	; C6 
			DCD	0x02007A1A	; C6 	;-
			DCD	0x02000000	; rest
			DCD	0x02007A1A	; C6 
			DCD	0x02007A1A	; C6 	;-
			DCD	0x04007A1A	; C6 
			
			DCD	0x04007A5A	; D6 
			
			DCD	0x04000000	; rest	;-
			DCD	0x1E005B56	; B5
			
									;-
			
									;-
									
									;-
									
									;-
			DCD	0x02000000	; rest	
			DCD	0x04007B56	; B5
			DCD	0x02003B16	; A5	;-
			DCD	0x02000000	; rest
			DCD	0x02000B16	; A5
			DCD	0x02000B16	; A5
			DCD	0x04000B16	; A5	;-
			DCD	0x04002B56	; B5
			DCD	0x04000000	; rest	;-
			DCD	0x04005B56	; B5
			DCD	0x04000000	; rest	;-
			DCD	0x0C004A1A	; C6
									;-
									
			DCD	0x08000000	; rest	;-
			
			DCD	0x020000FD	; G7	;-			
			DCD	0x020000BD	; F7
			DCD	0x0200007D	; E7	
			DCD	0x020000BD	; F7	
			DCD	0x0200009D	; E#7	;-
			DCD	0x0200007D	; E7
			DCD	0x0200005D	; D#7
			DCD	0x0200003D	; D
			
			DCD	0x0
			
; -------------------------------------------------
; =================================================

				END
				