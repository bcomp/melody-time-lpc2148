# MelodyTime: 1-Channel Chiptunes Tracker on LPC2148 Education Board

This project implements a framework for playing sequences of notes using a single, square-wave channel over the piezo-speaker built into the Embedded Artists LPC2148 Development Board.  The song written as data in `main.c` is a partial transcription of the title theme from the NES game, Megaman 2.

Being written in ARM7 assembly and targeting exceptionally limited hardware, posting it here is an exercise in posterity, although perhaps the simplicity of the code could provide a nice reference for someone implementing audio-related microcode to peruse. 
