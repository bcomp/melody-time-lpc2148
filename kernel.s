; Ben Companeitz
; ECE 425L - Dr. Mirzaei
; Final Project

; >>> kernel.s <<<
; Contains scheduling and interrupt configuration and 
; 	daemon routines that handle vectored IRQ tasks.
; For example, when TIMER1 is active, it triggers the
; 	'speakerd' vectored ISR at particular intervals,
; 	where it toggles the speaker-connected pin to
; 	generate a particular pitch.
; This method of frequency generation was selected so
;	that note duration might be handled concurrently
;	without unnecessary math/manual code interleaving.

				AREA	kernel, CODE, READONLY
ENTRY

; ------------------------------------------------------
				GLOBAL	kernel_config
kernel_config	STMFA	sp!, {r0-r1,lr}
; ------------------------------------------------------
; Port Related Constants
PINSEL0			EQU	0xE002C000
PINSEL1			EQU	0xE002C004
; Port Base Addrs (DON'T USE ALONE!)
PORT0			EQU		0xE0028000
PORT1			EQU		0xE0028010
; Pin Manipulation Offsets
IODIR			EQU		0x8
IOSET			EQU		0x4
IOCLR			EQU		0xC
; Speaker Related Constants
speaker_pin		EQU		0x2000000	;p0.25

				LDR	r0, =PINSEL1
				LDR	r1, [r0]
				BIC	r1, r1, #0x000C0000	; p0.25	= GPIO
				STR	r1, [r0]
				
				LDR	r0, =PORT0
				MOV	r1, #speaker_pin
				STR	r1, [r0, #IODIR]

; ------------------------------------------------------
; VIC Related Constants
VICIntSelect 	EQU	0xFFFFF00C		; FIQ (1) or IRQ (0)?
									; bit 14-17: EINT0-3
									; bit4: TIMER0
									; bit5: TIMER1
									; bit8: PWM0
									; same for en/enclr
VICIntEnable	EQU	0xFFFFF010		; Haz interroopt?
VICIntEnClr		EQU	0xFFFFF014     	; No haz nao... :<
VICCurVect		EQU	0xFFFFF030		; Which is the active vector
									; Must write bogus vector to it
									; to acknowledge it!!!!
VICVectAddr		EQU	0xFFFFF100		; There are 15, word indexed
VICVectCntl		EQU	0xFFFFF200		; Also 15, still word indexed
VICDefVectAddr	EQU	0xFFFFF034		; For non-vectored stuff...

;VIC_cfg		; This sets which types of interrupts
				; and where their vectors are.

				LDR	r0, =VICIntSelect
				MOV	r1, #0x0
				STR	r1, [r0]	; All are IRQ! (unecessary...)

				LDR	r0, =VICDefVectAddr
				;ADR	r1, irq_nonvectored
				MOV	r1, #0x8	; Send it to SWI because I'm lazy
				STR	r1, [r0]

				LDR	r0, =VICIntEnable
				MOV	r1, #0x30
				STR	r1, [r0]	; enable TIMER1/0 interrupt
				
				; Vectored interrupt order
				; 0: TIMER1 (Frequency generator)
				; 1: TIMER0 (Chord rippler)
INDEX_T0		EQU	0
INDEX_T1		EQU	4
				
				LDR	r0, =VICVectCntl
				MOV	r1, #0x25			; T1
				STR	r1, [r0, #INDEX_T0]
				LDR	r0, =VICVectCntl
				MOV	r1, #0x24			; T0
				STR	r1, [r0, #INDEX_T1]
				
				LDR	r0, =VICVectAddr
				ADR	r1, speakerd
				STR	r1, [r0, #INDEX_T0]
				LDR	r0, =VICVectAddr
				ADR	r1, rippled
				STR	r1, [r0, #INDEX_T1]

; ------------------------------------------------------
; INITIALIZE THINGS? HERE
; ------------------------------------------------------
				LDMFA	sp!, {r0-r1,pc}		
; END kernel_config
; ------------------------------------------------------


; DAEMON ROUTINES BELOW
; ======================================================
; ------------------------------------------------------
; TIMER1 Related Constants
TIMER1IR 		EQU	0xE0008000  ; Interrupt Registers
TIMER0IR        EQU 0xE0004000  
; Music Related Constants
CURRENT_LINE	DCD	0x40007FFC
CURRENT_NOTE	DCD	0x40007FF8

; ------------------------------------------------------
; SUBROUTINE: IRQ for Timer1, which handles freq gen
speakerd	STMFA	sp!, {r0-r2,lr}
			; Get which match, port addr, and pin mask
			LDR	r0, =TIMER1IR
			LDR	r1, [r0]
			LDR	r0, =PORT0
			MOV	r2, #speaker_pin
			; Check which one, do the thing
			TST	r1, #0x1
			STRNE	r2, [r0, #IOSET]
			TST	r1, #0x2
			STRNE	r2, [r0, #IOCLR]
			; Gracefully exit
exit_s		LDR	r0, =TIMER1IR
			STR	r1, [r0]	; Clear Timer IR
			LDR	r2, =VICCurVect
			STR	sp, [r2]	; Acknowledge VIC vector w/bogus val
			LDMFA	sp!, {r0-r2,lr}
			SUBS	pc, lr, #4
; END SUBROUTINE	

			IMPORT	R2_get_cycles
			IMPORT	press_note
; ------------------------------------------------------
; SUBROUTINE: rippled
rippled		STMFA	sp!, {r0-r3,lr}
			; Check MS bit of current_line
			LDR	r3, CURRENT_LINE
			LDR	r0, [r3]
			TST	r0, #0x80000000
			; If none, set and reset current_count (might be unnec)
			MOV	r1, #0
			ORREQ	r0, r0, #0x80000000
			STREQ	r0, [r3]
			STREQ	r1, [r3, #-4]
			; Based on count, shift to correct note/octave
			LDR	r1, [r3, #-4]	; get counter
			ADD	r1, r1, #1		; or increment
			AND	r2, r0, #0x3	; isolate # notes
			CMP	r1, r2
			MOV	r2, #7			; prepare for shift...
			MOVGE	r1, #0		; counter wraparound
			STR	r1, [r3, #-4]	; save counter
			MUL	r2, r1, r2		; shift number
			MOV	r0, r0, LSR r2
			; Load to r0
			; Iterate current_count, looping if equal to 
			;; the first 2 bits of current_line
			MOV	r3, #12
			MOV	r1, r0, LSR #2	; shift out "# notes"
			AND	r1, r1, #0x7	; mask octave
			MOV	r2, r0, LSR	#5
			AND	r2, r2, #0xF	; mask note
			MUL	r1, r3, r1		; calculate octave offset
			ADD r1, r2, r1		; add note offset
			BL	R2_get_cycles	; get cycles for note
			BL	press_note
			; Gracefully exit
exit_r		LDR	r0, =TIMER0IR
			LDR	r1, [r0]
			STR	r1, [r0]	; Clear Timer IR
			LDR	r2, =VICCurVect
			STR	sp, [r2]	; Acknowledge VIC vector w/bogus val
			LDMFA	sp!, {r0-r3,lr}
			SUBS	pc, lr, #4
; END SUBROUTINE


; ------------------------------------------------------
; SUBROUTINE: centerd
centerd		STMFA	sp!, {r2-r3,lr}
			
			LDMFA	sp!, {r2-r3,pc}
; END SUBROUTINE

; ------------------------------------------------------
; SUBROUTINE: downd
downd		STMFA	sp!, {r2-r3,lr}
			
			LDMFA	sp!, {r2-r3,pc}
; END SUBROUTINE

; ------------------------------------------------------
			END
			