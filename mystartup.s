; Ben Companeitz
; ECE 425 - Dr. Mirzaei
; Final Project



; >>> startup.s <<<
; In addition to defining the standard ARM vectors,
; 	the included reset handler sets up several stacks
; 	and runs configuration code for the kernel and
; 	instrument before entering the main song.

				AREA RESET, CODE, Readonly
ENTRY

ARM
				
				IMPORT main_code
                
VECTORS
				LDR PC,Reset_Addr
				LDR PC,Undef_Addr
				LDR PC,SWI_Addr
 				LDR PC,PAbt_Addr
				LDR PC,DAbt_Addr
				NOP
				LDR PC,[PC,#-0x0FF0]	; VICVectAddr
				;LDR	PC,IRQ_Addr
				LDR	PC,FIQ_Addr
		
Reset_Addr		DCD ResetHandler
Undef_Addr		DCD UndefHandler
SWI_Addr		DCD SWIHandler
PAbt_Addr		DCD PAbtHandler
DAbt_Addr		DCD DAbtHandler
				DCD 0
IRQ_Addr		DCD IRQHandler
FIQ_Addr		DCD FIQHandler

; -------------------------------------------------
; Mode and Interrupt Bits
Mode_USR		EQU 0x10
Mode_FIQ		EQU 0x11
Mode_IRQ		EQU	0x12
I_Bit			EQU 0x80	;when set, IRQ disabled
F_Bit			EQU 0x40	;when set, FIQ disabled

; Stack Related Constants
SRAM_Base		EQU 0x40000000
USR_Stack_Off	EQU 0x00000100
FIQ_Stack_Off	EQU	0x00000000
IRQ_Stack_Off	EQU	0x00000200
; -------------------------------------------------

ResetHandler
			; SET THE FIQ STACK UP
			MOV	r14, #Mode_FIQ
			ORR	r14, r14, #(I_Bit+F_Bit)		; Not interrupting!
			MSR	cpsr_c, r14
			LDR	sp, =SRAM_Base+FIQ_Stack_Off	; FD stack(notrly)
			
			; SET UP IRQ STACK
			MOV	r14, #Mode_IRQ
			ORR	r14, r14, #(I_Bit)
			MSR	cpsr_c, r14
			LDR	sp, =SRAM_Base+IRQ_Stack_Off	; FD stack(notrly)
			
			; USER STUFF
			MOV	r14, #Mode_USR
			BIC	r14, r14, #(I_Bit+F_Bit)
			MSR	cpsr_c, r14						; Enter usermode
			LDR	sp, =SRAM_Base+USR_Stack_Off	; FD stack
					
			; CONFIGURE EVERYTHING
			IMPORT	kernel_config
			BL	kernel_config
			IMPORT	instr_config
			BL	instr_config

			B	main_code

UndefHandler	B UndefHandler
SWIHandler		B SWIHandler
PAbtHandler		B PAbtHandler
DAbtHandler		B DAbtHandler
IRQHandler		B IRQHandler
FIQHandler		B FIQHandler

				END
